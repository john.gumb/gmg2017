################################
# Green meets Grey Workshop 2017
################################

import machine
import pycom
import time
from gmg.common import blink, Colour
from gmg.connections import connect_to_raw_lora
from gmg.sensors import initialise_sensor_board, SensorBoard


pycom.heartbeat(False)

# initialise raw LoRa and PySense board
lora = connect_to_raw_lora()
pysense = initialise_sensor_board(SensorBoard.PYSENSE)


while True:
    blink(Colour.GREEN, 3, 0.5)  # heartbeat

    # Accelerometer
    pitch = pysense.pitch()
    roll = pysense.roll()
    accel = pysense.acceleration()
    print('45BC: Your pitch/roll degrees, and 3 directional G forces are: '
          '%r, %r, X=%r, Y=%r, Z=%r' % (pitch, roll, accel[0], accel[1], accel[2]))
    lora.send('45BC: Pitch/roll data in degrees is: %r, %r' % (pitch, roll))
    lora.send('45BC: Accelerometer data in G is: X=%r, Y=%r, Z=%r' % (accel[0], accel[1], accel[2]))

    # Humidity
    humidity = pysense.humidity()
    print('45BC: Humidity data in percent is: %r' % humidity)
    lora.send('45BC: Humidity data in percent is: %r' % humidity)

    # Temperature
    temp = pysense.temperature()
    print('45BC: Temperature data in celcius is: %r' % temp)
    lora.send('45BC: Temperature data in celcius is: %r' % temp)

    # Light
    lightlevels = pysense.light()
    print('45BC: Light data in lux is: %r %r' % (lightlevels[0], lightlevels[1]))
    lora.send('45BC: Light data in lux is: %r %r' % (lightlevels[0], lightlevels[1]))

    # Barometry
    pressure = pysense.pressure()
    print('45BC: Pressure data in pascals is: %r' % pressure)
    lora.send('45BC: Pressure in pascals data is: %r' % pressure)

    altitude = pysense.altitude()
    print('45BC: Altitude data in metres is: %r' % altitude)
    lora.send('45BC: Altitude in metres data is: %r' % altitude)

    
    # wait for a random time
    time.sleep(machine.rng() & 0x0F)
