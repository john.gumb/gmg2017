################################
# Green meets Grey Workshop 2017
################################

# first add any imports that are needed
import machine
import time
from gmg.common import blink, Colour
from gmg.connections import connect_to_raw_lora

# connect to LoRa
lora = connect_to_raw_lora()

while True:
    blink(Colour.GREEN, 3, 0.5)  # blink the LED green 3 times

    # send some data
    print('Attempting to send: Hello room from Team xxxxxx')
    lora.send('Hello room from Team xxxxxx')

    # receive & filter some data
    data = lora.receive(64)
    if 'Entry code for Team xxxxxx:' in data:
        print(data)

    # wait for a random length of time
    time.sleep(machine.rng() & 0x0F)
