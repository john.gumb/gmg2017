################################
# Green meets Grey Workshop 2017
################################

import machine
import pycom
import time
from gmg.common import blink, Colour
from gmg.connections import connect_to_raw_lora
from gmg.sensors import initialise_sensor_board, SensorBoard


pycom.heartbeat(False)

# initialise raw LoRa and PyTrack board
lora = connect_to_raw_lora()
pytrack = initialise_sensor_board(SensorBoard.PYTRACK)


while True:
    blink(Colour.GREEN, 3, 0.5)  # heartbeat

    # Accelerometer
    pitch = pytrack.pitch()
    roll = pytrack.roll()
    accel = pytrack.acceleration()
    print('45BC: Your pitch/roll degrees, and 3 directional G forces are: '
          '%r, %r, X=%r, Y=%r, Z=%r' % (pitch, roll, accel[0], accel[1], accel[2]))
    lora.send('45BC: Pitch/roll data in degrees is: %r, %r' % (pitch, roll))
    lora.send('45BC: Accelerometer data in G is: X=%r, Y=%r, Z=%r' % (accel[0], accel[1], accel[2]))


    # GPS
    latitude, longitude = pytrack.coordinates()
    print('45BC: Latitude: %r, Longitude: %r' % (latitude, longitude))
    lora.send('45BC: Latitude: %r, Longitude: %r' % (latitude, longitude))

    # wait for a random time
    time.sleep(machine.rng() & 0x0F)
