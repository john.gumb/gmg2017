################################
# Green meets Grey Workshop 2017
################################

import binascii
import machine
import pycom
import time
from gmg.common import blink, Colour, mac_hex_lora
from gmg.connections import connect_to_lora
from gmg.sensors import initialise_sensor_board, SensorBoard


pycom.heartbeat(False)
app_eui = binascii.unhexlify('70 B3 D5 7E F0 00 4B DB'.replace(' ',''))
app_key = binascii.unhexlify('D6 76 00 71 F9 1C A5 F9 6A 95 13 99 3F 1A 58 80'.replace(' ',''))

# attempt LoRa connection - repeat until successful
lora = None
while lora is None:
    lora = connect_to_lora(app_eui, app_key, timeout=30)

# initialise PYSENSE sensor board
pysense = initialise_sensor_board(SensorBoard.PYSENSE)


while True:
    blink(Colour.GREEN, 3, 0.5)  # heartbeat

    # check connection is still live, repeated re-attempt connection if failed
    while not lora.is_connected():
        print('Refreshing connection...')
        lora.connect(timeout=30)


    # use this to look up your results on the database - remove the ':'
    print('Your DevEUI is: %r' % mac_hex_lora)
    
    # read sensor values
    temp = pysense.temperature()
    humidity = pysense.humidity()

    # send data
    print('45BC: temperature = %r, humidity = %r' % (temp, humidity))
    lora.send('45BC: temperature = %r, humidity = %r' % (temp, humidity))

    time.sleep(30)
