import machine
import pycom
import time
from gmg.common import blink, Colour
from gmg.connections import connect_to_raw_lora


pycom.heartbeat(True)
lora = connect_to_raw_lora()

team_keys = {
    'Baked Beans': '0000',
    'Bobs Burgers': '1111'
}    

while True:

    # listen
    data = lora.receive(256)
    print(data)

    # check incoming data for team names, send out key if found
    for team_name in team_keys:
        if team_name in data:
            lora.send('Entry code for Team %s: %s' % (
                team_name, team_keys[team_name]))

    time.sleep(1)
