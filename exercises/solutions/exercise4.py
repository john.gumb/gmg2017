################################
# Green meets Grey Workshop 2017
################################

import binascii
import crypto
import machine
import pycom
import time
from gmg.common import blink, Colour
from gmg.connections import connect_to_lora
from gmg.sensors import initialise_sensor_board, SensorBoard


app_eui = binascii.unhexlify('70 B3 D5 7E F0 00 4B DB'.replace(' ',''))
app_key = binascii.unhexlify('D6 76 00 71 F9 1C A5 F9 6A 95 13 99 3F 1A 58 80'.replace(' ',''))

# attempt LoRa connection - repeat until successful
lora = None
while lora is None:
    lora = connect_to_lora(app_eui, app_key, timeout=30)

# initialise PYSENSE sensor board
pytrack = initialise_sensor_board(SensorBoard.PYTRACK)
if pytrack is None:
    exit()

# create encryption key, must be 16 bytes long (128 bit)
crypto_key = b'notsuchsecretkey'


while True:
    blink(Colour.GREEN, 3, 0.5)  # heartbeat

    # check connection is still live, repeated re-attempt connection if failed
    while not lora.is_connected():
        print('Refreshing connection...')
        lora.connect(timeout=30)

    # read sensor values
    coords = pytrack.coordinates()

    # encrypt data
    iv = crypto.getrandbits(128)  # hardware generated random IV (never reuse it)
    cipher = crypto.AES(crypto_key, crypto.AES.MODE_CFB, iv)
    message = iv + cipher.encrypt(b'Super secret data! Coordinates: longitude=%r, latitude=%r' % (coords[0], coords[1]))

    # send data
    print('45BC: Super secret data! Coordinates: longitude=%r, latitude=%r' % (coords[0], coords[1]))
    print('45BC: Encrypted super secret message: %r' % message)
    lora.send(message)

    time.sleep(30)


# On the decryption side: (FROM https://pypi.python.org/pypi/pycrypto)
#   from Crypto.Cipher import AES
#   ciper = AES.new(crypto_key, AES.MODE_CFB, iv)
#   message =  ciper.decrypt(encrypted_message)
