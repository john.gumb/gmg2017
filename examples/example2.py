import pycom
import time
import machine

from gmg.connections import connect_to_raw_lora
from gmg.common import Colour, blink

LOPY_NAME = 'Jango Fett'  # enter name here

# initialise raw LoRa connection
connection = connect_to_raw_lora()

while True:
    blink(Colour.GREEN, 3, 1)  # blink the LED green 3 times, 1 second delay between each
    pycom.rgbled(Colour.BLUE)  # set to LED to blue

    # send hello message
    connection.send('Hello from %s' % LOPY_NAME)

    # check if any data have been received
    incoming = connection.receive(64, blocking=False)
    print('Data received: %s' % incoming)

    # wait a random amount of time
    time.sleep(machine.rng() & 0x0F)
