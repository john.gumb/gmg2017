import binascii
import pycom
import time

from gmg.connections import connect_to_lora
from gmg.common import Colour, blink
from gmg.sensors import initialise_sensor_board, SensorBoard


# create an OTAA authentication parameters
dev_eui = binascii.unhexlify('00 86 6A 8E 08 9B C1 CD'.replace(' ',''))
app_eui = binascii.unhexlify('70 B3 D5 7E F0 00 4B DB'.replace(' ',''))
app_key = binascii.unhexlify('D6 76 00 71 F9 1C A5 F9 6A 95 13 99 3F 1A 58 80'.replace(' ',''))

# attempt LoRa connection - repeat until successful
connection = None
while connection is None:
    connection = connect_to_lora(app_eui, app_key, timeout=30)

# initialise PYSENSE sensor board
pysense = initialise_sensor_board(SensorBoard.PYSENSE)
if pysense is None:
    exit()

while True:
    blink(Colour.GREEN, 3, 1)  # blink the LED green 3 times, 1 second delay between each
    pycom.rgbled(Colour.BLUE)  # set to LED to blue

    # check connection is still live - repeat re-attempt unjtil success
    while not connection.is_connected():
        print('Refreshing connection...')
        connection.connect(timeout=30)

    # send hello world
    datastr = 'hello world'
    print('Attempting to send: %s' % datastr)
    connection.send(datastr)

    # print sensor values
    print('roll: %r' % pysense.roll())
    print('pitch %r' % pysense.pitch())
    print('acceleration: %r' % [a for a in pysense.acceleration()])
    print('light: %r' % [a for a in pysense.light()])
    print('humidity: %r' % pysense.humidity())
    print('temperature: %r' % pysense.temperature())
    print('pressure: %r' % pysense.pressure())

    time.sleep(30)
