import machine
from network import WLAN
import os
import pycom
import time

from gmg.common import mac_hex_wlan, blink, Colour

# found in default boot.py
uart = machine.UART(0, 115200)
os.dupterm(uart)

# set up WiFi AP - only restart the access point on a HARD RESET
if machine.reset_cause() != machine.SOFT_RESET:
    ssid_suffix = ''.join(mac_hex_wlan().split(':')[-2:])
    wlan = WLAN(mode=WLAN.AP)
    wlan.init(mode=WLAN.AP, ssid='lopy-wlan-%s' % ssid_suffix,
              auth=(WLAN.WPA2, 'www.pycom.io'))

# must disable the LED heartbeat to allow manual control of the LED
pycom.heartbeat(False)
time.sleep(3)
