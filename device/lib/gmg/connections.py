from network import LoRa, WLAN
import pycom
import socket as usocket
import time

from gmg.common import Colour


def connect_to_raw_lora():
    '''
    Initialises LoRaWAN without connecting to a gateway, allowing for local data
    transfer.
    :returns: a LoraConnection instance
    '''
    print('Initialising raw LoRaWAN...')
    pycom.rgbled(Colour.RED)  # set LED to red

    connection = LoRaConnection(app_eui='', app_key='', local=True)
    connection.connect()

    print('Raw LoRaWAN initialised!')
    pycom.rgbled(Colour.GREEN)  # set LED to green
    return connection


def connect_to_lora(app_eui, app_key, timeout=30):
    '''
    Initialises a LoRaWAN connection to a gateway. Performs connection
    boilerplate, such as timeouts, printing status, and checking if the
    connection was successful.
    :param str app_eui: the AppEUI value used to connect to the gateway/registrar
    :param str app_key: the AppKey value used to connect to the gateway/registrar
    :param float timeout: seconds to wait before the connection attempt stops
    :returns: a LoRaConnection instance
    '''
    print('Attempting to connect to LoRaWAN...')
    pycom.rgbled(Colour.RED)  # set LED to red

    connection = LoRaConnection(app_eui, app_key, local=False)
    try:
        connection.connect(timeout=timeout)
    except Exception as err:
        # FIXME: specifically checking for TimeoutError throws error
        print('LoRaWAN connection attempt timed out!')
        return None
    
    if not connection.is_connected():
        print('LoRaWAN connection attempt failed!')
        return None

    print('Successfully connected to LoRaWAN!')
    pycom.rgbled(Colour.GREEN)  # set LED to green
    return connection

# FIXME: uncomment after WiFiConnection class is fixed
# def connect_to_wifi(ssid, auth):
#     print('Attempting to connect to WiFi...')
#     pycom.rgbled(Colour.RED)  # set LED to red
    
#     connection = WiFiConnection(ssid, auth)
#     try:
#         connection.connect(30)
#     except Exception as err:
#         print('WiFi connection attempt timed out!\n%r' % err)
#         return None

#     if not connection.is_connected():
#         print('WiFi connection attempt failed!')
#         return None

#     print('Successfully connected to LoRaWAN!')
#     pycom.rgbled(Colour.GREEN)  # set LED to green
#     return connection


class Connection(object):
    '''
    Generic connection superclass.
    '''
    def __init__(self, socket=None):
        self.socket = socket
    
    def __enter__(self):
        if not self.is_connected:
            self.connect()
    
    def __exit__(self, exc_type, exc_alue, exc_traceback):
        if self.is_connected:
            self.disconnect()

    def connect(self, timeout=0):
        pass

    def disconnect(self):
        '''
        Disconnects the connection.
        '''
        if self.socket is not None:
            self.socket.close()

    def is_connected(self):
        '''
        :returns: bool, True if connection is up
        '''
        return False
    
    def send(self, data, delay=0.1):
        '''
        Sends the given data on the open socket. Allows data to either be a
        single string, or a list of strings to be sent individually.
        :param list/str data: the data to be sent, messages must be <256 characters
        :param float delay: seconds between sending each message in data, if list
        :returns: boolean, True if successful
        '''
        # allow non-list input
        if not isinstance(data, list):
            data = [data]

        for payload in data:
            # convert to string
            if not isinstance(payload, str):
                payload = str(payload)

            # check length of payload - arbitrary, it matches the length of the
            # payload_ascii field in the AWS database
            if len(payload) >= 512:
                print('Failed to send! data = %r\nError: Too much data!'
                      'Messages must be <512 characters, was %r.' % data, len(payload))
                return False

        self.socket.setblocking(True)
        try:
            for payload in data:
                self.socket.send(payload)
                time.sleep(delay)
        except Exception as err:
            print('Failed to send! data = %r\nError: %r' % (data, err))
            return False
        else:
            return True
        finally:
            self.socket.setblocking(False)

    def receive(self, max_size, blocking=False, timeout=10):
        '''
        Receives data on the socket.
        :param int max_size: maximum number of bytes to receive (recommended to
        be a power of 2, e.g. 64)
        :param bool blocking: whether to wait until data is received/timeout
        reached
        :param float timeout: if blocking, the number of seconds to wait until
        data is received
        :returns: bytes received
        '''
        self.socket.setblocking(blocking)

        if not blocking:
            return self.socket.recv(max_size)

        self.socket.settimeout(timeout)
        data = self.socket.recv(max_size)
        self.socket.setblocking(False)
        return data


class LoRaConnection(Connection):
    '''
    Encapsulates a LoRaWAN connection. Inherits the Connection superclass.
    '''
    def __init__(self, app_eui, app_key, local):
        '''
        :param str app_eui: the AppEUI value used to connect to the gateway/registrar
        :param str app_key: the AppKey value used to connect to the gateway/registrar
        :param bool local: flag to indicate whether it should attempt to connect
        to a gateway
        '''
        mode = LoRa.LORA if local else LoRa.LORAWAN
        self.lora = LoRa(mode=mode)
        self.dev_eui = self.lora.mac()
        self.app_eui = app_eui
        self.app_key = app_key
        self.local = local
    
    def connect(self, timeout=30):
        '''
        Attempts to connect to LoRaWAN, if local=False then it attempts
        connecting to a gateway. Handles connection boilerplate, such as socket
        setup, and timeout.
        :param float timeout: if local=False, number of seconds before connection
        attempt is aborted, after which a TimeoutError is raised.
        '''
        if not self.local:
            # timeout raises TimeoutError
            self.lora.join(activation=LoRa.OTAA, auth=(self.dev_eui, self.app_eui, self.app_key), timeout=timeout*1000)

        # if successful, create a LoRa socket and configure LoRaWAN data rate
        self.socket = usocket.socket(usocket.AF_LORA, usocket.SOCK_RAW)
        if not self.local:
            self.socket.setsockopt(usocket.SOL_LORA, usocket.SO_DR, 5)

    def is_connected(self):
        '''
        :returns: bool, True if the connection is up
        '''
        if not self.local:
            return self.lora.has_joined()
        else:
            return self.socket is not None


# FIXME: this whole class
# class WiFiConnection(Connection):
#     def __init__(self, ssid, auth):
#         self.wlan = WLAN(mode=WLAN.STA)  # defaults to AP (access point) mode
#         self.ssid = ssid
#         self.auth = auth
        
#     def connnect(self, timeout=30):
        
#         available_networks = self.wlan.scan()
#         print('Available networks: %r' % available_networks)
        
#         if self.ssid not in [a.ssid for a in available_networks]:
#             raise ValueError('SSID \"%s\" not found' % self.ssid)
        
#         #self.wlan.connect(ssid=self.ssid, auth=(WLAN.WPA2, self.password), timeout=timeout*1000)
#         self.wlan.connect(ssid=self.ssid, auth=self.auth, timeout=timeout*1000)
        
#         if not self.is_connected():
#             self.disconnect()
#             raise RuntimeError('Wifi connection timed out!')
        
#         print('Connecton details: %r' % [a for a in self.wlan.ifconfig()])

#         # TODO: connect to socket
        

#     def disconnect(self):
#         return False

#     def is_connected(self):
#         return self.wlan.isconnected()
