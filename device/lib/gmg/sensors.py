from lib.pycom.L76GNSS import L76GNSS  # GPS
from lib.pycom.LIS2HH12 import LIS2HH12  # 3-Axis Accelerometer
from lib.pycom.LTR329ALS01 import LTR329ALS01  # Ambient Light
from lib.pycom.MPL3115A2 import MPL3115A2  # Barometric Pressure & Altitude
from lib.pycom.SI7006A20 import SI7006A20  # Humidity & Temperature

from lib.pycom.pytrack import Pytrack
from lib.pycom.pysense import Pysense

# try:
#     Pysense()
#     from lib.pycom.LIS2HH12 import LIS2HH12  # 3-Axis Accelerometer
#     from lib.pycom.LTR329ALS01 import LTR329ALS01  # Ambient Light
#     from lib.pycom.MPL3115A2 import MPL3115A2  # Barometric Pressure & Altitude
#     from lib.pycom.SI7006A20 import SI7006A20  # Humidity & Temperature
# except Exception as e:
#     pass  # PySense board not detected

# try:
#     Pytrack()
#     from lib.pycom.LIS2HH12 import LIS2HH12  # 3-Axis Accelerometer
#     from lib.pycom.L76GNSS import L76GNSS  # GPS
# except Exception as e:
#     pass  # PyTrack board not detected


def initialise_sensor_board(board_type):
    '''
    Initialises the sensor board.
    If the given board_type does not match that physically connected to the LoPy
    then initialisation fails.
    Once successfully called, it should not be called again with another board
    type, as only one type of board can be connected to the LoPy at once.
    :param int board_type: either SensorBoard.PYTRACK, or SensorBoard.PYSENSE
    :returns: SensorBoard instance, either PyTrack or PySense specific. None if
    initialisation fails.
    '''
    try:
        if board_type == SensorBoard.PYTRACK:
            return PyTrack()
        elif board_type == SensorBoard.PYSENSE:
            return PySense()
    except Exception as err:
        print('Error initialising sensor board!\nError: %r' % err)
        return None

    return None


class SensorBoard(object):
    '''
    Generic sensor board superclass.
    Singleton, once initialised it cannot be reinitialised to another board type.
    '''
    PYTRACK = 0  # Enum for the PYTRACK sensor board type
    PYSENSE = 1  # Enum for the PYSENSE sensor board type
    board = None  # pytrack/pysense board instance
    board_type = None  # type of board initialised, either PYTRACK or PYSENSE

    def __init__(self, board_type):
        if self.board_type == board_type:
            return  # already initialised
        else:
            if self.has_pytrack() or self.has_pysense():
                raise RuntimeError('Cannot change the type of board after initialisation')

        if board_type == SensorBoard.PYTRACK:
            SensorBoard.board = Pytrack()
        elif board_type == SensorBoard.PYSENSE:
            SensorBoard.board = Pysense()
        else:
            raise ValueError('Invalid sensor board type given')

        SensorBoard.board_type = board_type

    @classmethod
    def has_pytrack(cls):
        return cls.board_type == cls.PYTRACK

    @classmethod
    def has_pysense(cls):
        return cls.board_type == cls.PYSENSE


class PyTrack(SensorBoard, LIS2HH12, L76GNSS):
    '''
    Encapsulates all pytrack sensors - GPS, and 3-Axis accelerometer.
    Sensor calls include:
        roll(), pitch(), acceleration(), coordinates()
    See pycom docs for further information:
        https://docs.pycom.io/chapter/pytrackpysense/apireference/pytrack.html
    '''
    def __init__(self):
        SensorBoard.__init__(self, SensorBoard.PYTRACK)
        if not self.has_pytrack():
            raise RuntimeError('Cannot use PyTrack if pytrack board is not in use')

        LIS2HH12.__init__(self, self.board)
        L76GNSS.__init__(self, self.board, timeout=10)
    
    def coordinates(self):
        try:
            return L76GNSS.coordinates(self, debug=True)
        except MemoryError:
            return (None, None)


class PySense(SensorBoard, LIS2HH12, SI7006A20, MPL3115A2, LTR329ALS01):
    '''
    Encapsulates all pysense sensors -
        3-Axis Accelerometer, Ambient Light, Humidity, Temperature, Pressure
    Sensor calls include:
        roll(), pitch(), acceleration(), light(), humidity, temperature(), pressure()
    See pycom docs for futher information:
        https://docs.pycom.io/chapter/pytrackpysense/apireference/pysense.html
    '''
    def __init__(self):
        SensorBoard.__init__(self, SensorBoard.PYSENSE)
        if not self.has_pysense():
            raise RuntimeError('Cannot use PySense if pysense board is not in use')

        LIS2HH12.__init__(self, self.board)
        SI7006A20.__init__(self, self.board)
        MPL3115A2.__init__(self, self.board)  # mode defaults to PRESSURE
        LTR329ALS01.__init__(self, self.board)

        self.barometry_altitude = MPL3115A2(self.board, mode=0)

    def temperature(self):
        '''
        Forces temperature reading from the Humidty/Temperature sensor (SI7006A20),
        instead of the Barometric Pressure sensor (MPL3115A2).
        '''
        return SI7006A20.temperature(self)

    def altitude(self):
        '''
        As the inherited Barometric sensor instance defaults to pressure, this
        provides an explicit call to the altitude method of an alternative
        Barometry instance.
        '''
        return self.barometry_altitude.altitude()

