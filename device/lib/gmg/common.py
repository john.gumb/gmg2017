import pycom
import time
import ubinascii
from network import LoRa, WLAN


class Colour(object):
    '''
    Provides contants for hex colours, for use with pycom.rgbled.
    '''
    NONE = 0x000000
    WHITE = 0x111111
    RED = 0x110000
    GREEN = 0x001100
    BLUE = 0x000011
    BRIGHT_RED = 0xFF0000
    BRIGHT_GREEN = 0x00FF00
    BRIGHT_BLUE = 0x0000FF


def blink(col, rep, pause):
    '''
    Blinks the LED on the pycom a set number of times between Colour.NONE and
    the given colour.
    :param hex col: colour to flash to LED
    :param int rep: number of times to flash
    :param float pause: number of seconds to wait between changing colours
    '''
    for i in range(rep):
        pycom.rgbled(Colour.NONE)
        time.sleep(pause)
        pycom.rgbled(col)
        time.sleep(pause)


def mac_hex_wlan():
    '''
    Returns the WLAN mac address, converted to readable hex.
    '''
    return ubinascii.hexlify(WLAN().mac(), ':').decode()


def mac_hex_lora():
    '''
    Returns the the LoRaWAN mac address, converted to readable hex.
    '''
    return ubinascii.hexlify(LoRa().mac(), ':').decode()
