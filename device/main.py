################################
# Green meets Grey Workshop 2017
################################
# everything after a # is a comment, and isn’t treated as code


# first add any imports that are needed


# initialise things here


# connect to LoRa


# begin your program loop
while True:
    # do some stuff in the loop





    # the loop finishes...
# ...where the indentation returns to the previous level
# Be VERY CAREFUL WITH YOUR INDENTS
