
## Directory Contents
- **device:** this directory should be uploaded to the LoPy to provide all required libraries and boot code
    - **lib:**
        - **gmg:** (named for Green Meets Grey) contains boilerplate for common connection and sensor functions, allowing for simplified/high level interaction with the LoPy device
        - **pycom:** contains the pycom sensor libraries
- **examples:** code giving examples of how to use the provided libraries
- **exercises:** documents containing each exercise, and other related resources, such as solutions
    - **solutions:** example of what `main.py` could look like at the end of each exercise

___

## Connection Information
When the LoPy is powered on, it creates a WFi access point. 
- SSID: `lopy-wlan-<last 4 digits of WiFi mac address>`
- Password: `www.pycom.io`

Once connected to it, you can transfer data to the device.
- IP Address: `192.168.4.1`
- username: `micro`
- password: `python`

`device/boot.py` contains some configuration for the WiFi access point.

___

## Uploading code to the LoPy
In Atom/VSCode open this directory as the project. Then, in either `Global Settings` or `Project Settings`, set `Sync Folder` to "`device`". This means that when you click `Upload` or `Synchronise Project` only the `device` directory will be uploaded to the LoPy.

Generally, you'll only have to edit `device/main.py` - this file is executed after `boot.py` when a reset is performed.

- [pycom documentation](https://docs.pycom.io/chapter/pymakr/toolsfeatures.html#sync)


### FTP
As well as `Upload`/`Synchronise Project` you can use an FTP client, such as FileZilla, to directly transfer files to the LoPy. This also allows you to view the contents of the files on the LoPy, and generally debug upload issues.
- [FTP setup guide](https://docs.pycom.io/chapter/toolsandfeatures/FTP.html)

___

## AWS MySQL Database
- link: `http://rds-mysql-test.ctysulngdhwj.eu-west-2.rds.amazonaws.com/`
- port: 3306
- username: `share`
- password: `shareshare`

All incoming data from Actility is stored in the table `test.incoming`

___

## Additional Links
- [pycom docs](https://docs.pycom.io)
- [pymakr plugin](https://docs.pycom.io/chapter/pymakr/)
